
public class TicTacToe extends TurnBasedGame {

    //Game Board Representation
    //0|1|2
    //3|4|5
    //6|7|8
    private int[] gameBoard = {9, 9, 9, 9, 9, 9, 9, 9, 9};
    private int winner = -1;

    TicTacToe(Player player1, Player player2) {
        super(player1, player2);
        // TODO Auto-generated constructor stub
    }

    @Override
    //move is the index (location on board)

    protected void evaluateMove(String move) {
        // valid move detection
        int playerNum = isFirstPlayerTurn() ? 0 : 1;
        int moveSelection = Integer.parseInt(move);
        if (moveSelection > 8) {
            moveSelection = 8;
        } else if (moveSelection < 0) {
            moveSelection = 0;
        }
        if (gameBoard[moveSelection] == 9) { // attempt player's move
            gameBoard[moveSelection] = playerNum;
        } else { // pick another move for player
            int i;
            for (i = 0; i < 9; i++) {
                if (gameBoard[i] == 9) {
                    gameBoard[i] = playerNum;
                    break;
                }
            }
            if (i == 9) {
                winner = 2; // NO LEGAL MOVES LEFT
            }
        }
        
        // win detection
        if (isFirstPlayerTurn()) {
            //check for 0's in the board
            int zeroCounter = 0;
            for (int k = 0; k < 9; k++) {
                if (gameBoard[k] == 0) {
                    zeroCounter++;
                }
            }
            if (zeroCounter < 3) {
                // not enough moves to win
                // System.out.println("From method evaluate: not enough moves");
                return;
            }
            if (gameBoard[0] == 0) {
                if ((gameBoard[3] == 0 && gameBoard[6] == 0) || (gameBoard[1] == 0 && gameBoard[2] == 0) || (gameBoard[4] == 0 && gameBoard[8] == 0)) {
                    winner = 0;
                    return;
                }
            } else if (gameBoard[1] == 0) {
                if (gameBoard[4] == 0 && gameBoard[7] == 0) {
                    winner = 0;
                    return;
                }
            } else if (gameBoard[2] == 0) {
                if ((gameBoard[4] == 0 && gameBoard[6] == 0) || gameBoard[5] == 0 && gameBoard[8] == 0) {
                    winner = 0;
                    return;
                }
            } else if (gameBoard[3] == 0) {
                if (gameBoard[4] == 0 && gameBoard[5] == 0) {
                    winner = 0;
                    return;
                }
            } else if (gameBoard[6] == 0) {
                if (gameBoard[7] == 0 && gameBoard[8] == 0) {
                    winner = 0;
                    return;
                }
            }

        } else {
            //check for 1's in winning formation
            int oneCounter = 0;
            for (int k = 0; k < 9; k++) {
                if (gameBoard[k] == 1) {
                    oneCounter++;
                }
            }
            if (oneCounter < 3) {
                // not enough moves to win
                // System.out.println("From method evaluate: not enough moves");

            }
            if (gameBoard[0] == 1) {
                if ((gameBoard[3] == 1 && gameBoard[6] == 1) || (gameBoard[1] == 1 && gameBoard[2] == 1) || (gameBoard[4] == 1 && gameBoard[8] == 1)) {
                    winner = 1;
                    return;
                }
            } else if (gameBoard[1] == 1) {
                if (gameBoard[4] == 1 && gameBoard[7] == 1) {
                    winner = 1;
                    return;
                }
            } else if (gameBoard[2] == 1) {
                if ((gameBoard[4] == 1 && gameBoard[6] == 1) || gameBoard[5] == 1 && gameBoard[8] == 1) {
                    winner = 1;
                    return;
                }
            } else if (gameBoard[3] == 1) {
                if (gameBoard[4] == 1 && gameBoard[5] == 1) {
                    winner = 1;
                    return;
                }
            } else if (gameBoard[6] == 1) {
                if (gameBoard[7] == 1 && gameBoard[8] == 1) {
                    winner = 1;
                    return;
                }
            }

        }
    }

    @Override
    public void play() {
        System.out.println("=== Game Started ===\n");

        // while there is no winner, play turns
        while (winner == -1) {
            printBoard();
            playOneTurn();
        }

        // get name of winner and end game
        if (winner == 2) {
            System.out.println("There was a tie.");
        } else {
            String name = (winner < 1) ? player1.name : player2.name;
            System.out.println(name + " has won the game.");
        }
        System.out.println("=== Game Finished ===");
    }

    private void printBoard() {
        // print current state of board
        int index = 0;
        String c;
        for (int i = 0; i < 3; i++) {
            System.out.print("[ ");
            for (int j = 0; j < 3; j++) {
                if (gameBoard[index] == 9) {
                    c = " ";
                } else if (gameBoard[index] == 0) {
                    c = "X";
                } else {
                    c = "O";
                }
                System.out.print(c);
                index++;
            }
            System.out.println(" ]");
        }
    }

}
